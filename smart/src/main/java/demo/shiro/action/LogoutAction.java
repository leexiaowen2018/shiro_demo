package demo.shiro.action;

import org.smart4j.framework.mvc.annotation.Action;
import org.smart4j.framework.mvc.annotation.Request;
import org.smart4j.framework.mvc.bean.View;
import org.smart4j.security.SmartSecurityHelper;

@Action
public class LogoutAction {

    @Request.Get("/logout")
    public View logout() {
        // 获取当前用户并进行登出操作
        SmartSecurityHelper.logout();

        // 重定向到首页
        return new View("/");
    }
}
